Role Name
=========

Ansible role to deploy Grafana https://grafana.com/.

Requirements
------------

None.

Role Variables
--------------

TBD.

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: grafana }

License
-------

BSD
